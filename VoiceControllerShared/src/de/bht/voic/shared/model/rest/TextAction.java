package de.bht.voic.shared.model.rest;

import javax.persistence.Entity;

@Entity
public class TextAction extends Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5565135856351275219L;

	private String os;
	private String text;

	public TextAction() {
		// JPA
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
