package de.bht.voic.shared.model.rest;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OrderColumn;

@Entity
public class KeyAction extends Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 648949263786450775L;

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "VC_Action_Keycodes")
	@OrderColumn(name = "orderId")
	private List<Integer> keycodes;

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "VC_Action_Delays")
	@OrderColumn(name = "orderId")
	private List<Integer> delays;

	public KeyAction() {
		// JPA
	}

	public List<Integer> getKeycodes() {
		return keycodes;
	}

	public void setKeycodes(List<Integer> keycodes) {
		this.keycodes = keycodes;
	}

	public List<Integer> getDelays() {
		return delays;
	}

	public void setDelays(List<Integer> delays) {
		this.delays = delays;
	}
}
