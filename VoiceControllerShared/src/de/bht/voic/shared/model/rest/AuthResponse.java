package de.bht.voic.shared.model.rest;

public class AuthResponse {
	private String sessionToken;
	private String error;
	private User user;
	private UserInformation userInformation;

	public AuthResponse(final String sessionToken, final String error, final User user, final UserInformation userInformation) {
		this.sessionToken = sessionToken;
		this.error = error;
		this.user = user;
		this.userInformation = userInformation;
	}

	public AuthResponse() {
		// JSON
	}

	public AuthResponse(String sessionToken, String error) {
		this.sessionToken = sessionToken;
		this.error = error;
	}

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserInformation getUserInformation() {
		return userInformation;
	}

	public void setUserInformation(UserInformation userInformation) {
		this.userInformation = userInformation;
	}

	@Override
	public String toString() {
		return "AuthResponse [sessionToken=" + sessionToken + ", error=" + error + ", user=" + user + ", userInformation="
				+ userInformation + "]";
	}
}
