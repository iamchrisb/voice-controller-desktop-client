package de.bht.voic.shared.model.rest;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "VC_Userinformation")
public class UserInformation implements Serializable {

	private static final long serialVersionUID = -2294869834561470961L;

	@Id
	private BigInteger id;

//	@Column(unique=true)
	private String email;
	
	private BigInteger activeProfileId;

	public UserInformation() {
		// JPA
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public BigInteger getActiveProfileId() {
		return activeProfileId;
	}

	public void setActiveProfileId(BigInteger activeProfileId) {
		this.activeProfileId = activeProfileId;
	}

}
