package de.bht.voic.shared.model.rest;

import javax.persistence.Entity;

@Entity
public class CLIAction extends Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8451664894668088440L;
	
	private String exec;
	
	public CLIAction() {
		// JPA
	}

	public String getExec() {
		return exec;
	}

	public void setExec(String exec) {
		this.exec = exec;
	}
}
