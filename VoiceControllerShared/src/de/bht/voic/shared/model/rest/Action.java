package de.bht.voic.shared.model.rest;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Entity(name = "VC_ACTIONS")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = CLIAction.class, name = "cli"), @Type(value = KeyAction.class, name = "key"),
		@Type(value = TextAction.class, name = "text") })
public abstract class Action implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3282860429202805831L;

	@Id
	@GeneratedValue
	protected BigInteger id;

	//
	// @JoinTable
	// private Command command;

	public Action() {
		// JPA
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	// public Command getCommand() {
	// return command;
	// }
	//
	// public void setCommand(Command command) {
	// this.command = command;
	// }
}
