package de.bht.voic.shared.model.rest;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;

@Entity(name="VC_COMMAND")
public class Command implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	private BigInteger id;

	private String word;

	@OneToOne(cascade=CascadeType.ALL)
	private Action action;
	
//	@ElementCollection(fetch=FetchType.EAGER)
//	@CollectionTable(name="VC_Command_KeyCodes")
//	private Collection<Integer> keyCodes;
//
//	private Integer delayInMillis;
	

	private BigInteger profileId;

	public Command() {
		// JPA
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

//	public Collection<Integer> getKeyCodes() {
//		return keyCodes;
//	}
//
//	public void setKeyCodes(Collection<Integer> keyCodes) {
//		this.keyCodes = keyCodes;
//	}
//
//	public Integer getDelayInMillis() {
//		return delayInMillis;
//	}
//
//	public void setDelayInMillis(Integer delayInMillis) {
//		this.delayInMillis = delayInMillis;
//	}
//
	public BigInteger getProfileId() {
		return profileId;
	}

	public void setProfileId(BigInteger profileId) {
		this.profileId = profileId;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

}
