package de.bht.voic.client.common;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

public class RobotTextWriter {

	public void write(String os, String text) {

		Robot robot;
		try {
			robot = new Robot();
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			StringSelection str = new StringSelection(text);
			clipboard.setContents(str, str);

			if (os.equalsIgnoreCase("mac")) {
				robot.keyPress(KeyEvent.VK_META);
				robot.keyPress(KeyEvent.VK_V);
				robot.keyRelease(KeyEvent.VK_V);
				robot.keyRelease(KeyEvent.VK_META);
			} else if (os.equalsIgnoreCase("win")) {
				robot.keyPress(KeyEvent.VK_CONTROL);
				robot.keyPress(KeyEvent.VK_V);
				robot.keyRelease(KeyEvent.VK_V);
				robot.keyRelease(KeyEvent.VK_CONTROL);
			}

			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				System.out.println("Cannot interrupt thread");
			}
			str = new StringSelection("");
			clipboard.setContents(str, str);
		} catch (AWTException e) {
			System.out.println("Cannot initialize Robot.");
		}

	}
}
