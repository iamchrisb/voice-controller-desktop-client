package de.bht.voic.client.common;

import java.io.File;

public class Constants {
	public static final String API_KEY = "123456";
	public static final String HEADER_API_KEY = "api_key";
	public static final String HEADER_SESSION_TOKEN = "session_token";

	public static final String USERNAME_KEY = "username";
	public static final String PASSWORD_KEY = "password";

	public static final String HOST = "http://localhost:8080/";
	public static final String API = HOST + "VoiceControllerServer/";
	public static final String REST_API = API + "rest/";
	public static final String REST_API_AUTH = REST_API + "auth/";
	public static final String REST_API_AUTH_LOGIN = REST_API_AUTH + "login/form";
	public static final String ACTIVE_PROFILE_ID_KEY = "ACTIVE_PROFILE_ID";
	public static final String DEFAULT_PREFERENCE_VALUE = "no_available_key";
	public static final String USER_DATA_KEY = "USER_DATA_KEY";
	public static final String REST_API_PROFILE = REST_API + "profiles/";
	public static final String REST_API_COMMANDS = REST_API + "commands/";

	public static final String USER_DOC = System.getProperty("user.home") + File.separator + "Documents";
	public static final String VC_SETTINGS_FOLDER = USER_DOC + File.separator + "VoiceController" + File.separator;
	public static final String VC_SETTINGS_PROFILE = Constants.VC_SETTINGS_FOLDER + "active_profile.prof";

	public static final String GRAMMAR_NAME = "activeProfile";
	public static final String GRAMMAR_FILE_ENDING = ".gram";
	public static final String VC_GRAMMAR_PROFILE = Constants.VC_SETTINGS_FOLDER + GRAMMAR_NAME + GRAMMAR_FILE_ENDING;

	public static final String PROFILE_ID_KEY = "profileId";
	public static final String FILE_PROTOCOL = "file:" + File.separator + File.separator;

}
