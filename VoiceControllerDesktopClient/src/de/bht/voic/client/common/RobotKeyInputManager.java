package de.bht.voic.client.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RobotKeyInputManager {

	private Map<Integer, RobotKeyPressThread> keyPressThreads = new HashMap<Integer, RobotKeyPressThread>();

	private static final Integer RATE_IN_MILLIS = 60;
	private static final String THREAD_NAME_PREFIX = "KeyPressThread_";

	public void start(HashMap<Integer, Integer> keyInputs) {
		for (Map.Entry<Integer, Integer> entry : keyInputs.entrySet()) {
			Integer keycode = entry.getKey();
			Integer delay = entry.getValue();

			RobotKeyPressThread keyPressThread = new RobotKeyPressThread(THREAD_NAME_PREFIX + keycode, keycode, delay, RATE_IN_MILLIS);
			keyPressThread.start();
			keyPressThreads.put(keycode, keyPressThread);
		}
	}
	
	public void start(Collection<Integer> keycodes, Collection<Integer> delays) {
		List<Integer> keyCodeList = new ArrayList<Integer>(keycodes);
		List<Integer> delayList = new ArrayList<Integer>(delays);
		if(delays.size() != keycodes.size()) {
			System.out.println("size of delays is greater or less than keycodes. No key inputs triggered!");
			return;
		}
		for (int i = 0; i < keycodes.size(); i++) {
			Integer cKeycode = keyCodeList.get(i);
			Integer cDelay = delayList.get(i);
			RobotKeyPressThread keyPressThread = new RobotKeyPressThread(THREAD_NAME_PREFIX + cKeycode, cKeycode, cDelay, RATE_IN_MILLIS);
			keyPressThread.start();
			keyPressThreads.put(cKeycode, keyPressThread);
		}
	}
	
	public void start(Collection<Integer> keycodes, Integer delay) {
		List<Integer> keyCodeList = new ArrayList<Integer>(keycodes);
		for (int i = 0; i < keycodes.size(); i++) {
			Integer cKeycode = keyCodeList.get(i);
			RobotKeyPressThread keyPressThread = new RobotKeyPressThread(THREAD_NAME_PREFIX + cKeycode, cKeycode, delay, RATE_IN_MILLIS);
			keyPressThread.start();
			keyPressThreads.put(cKeycode, keyPressThread);
		}
	}

	public void stop(int[] keycodes) {
		for (int keycode : keycodes) {
			RobotKeyPressThread keyPressThread = keyPressThreads.get(keycode);
			keyPressThread.stop();
			keyPressThreads.remove(keycode);
		}
	}

	public void stop() {
		for (RobotKeyPressThread keyPressThread : keyPressThreads.values()) {
			keyPressThread.stop();
		}
		keyPressThreads.clear();
	}

}
