package de.bht.voic.client.common;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.prefs.Preferences;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import de.bht.voic.client.model.StoredUserData;

public class UserPreferenceStorage implements UserDataStorage {

	private Preferences prefs;

	public UserPreferenceStorage() {
		prefs = Preferences.userNodeForPackage(this.getClass());
	}

	@Override
	public void store(String sessionToken, String username, BigInteger activeProfileId) {
		HashMap<String, Object> userData = new HashMap<String, Object>();
		userData.put(Constants.USERNAME_KEY, username);
		userData.put(Constants.HEADER_SESSION_TOKEN, sessionToken);

		if (activeProfileId != null) {
			userData.put(Constants.ACTIVE_PROFILE_ID_KEY, activeProfileId.toString());
		} else {
			System.err.println("You have no active profile set!");
		}

		JSONObject userJSON = new JSONObject(userData);

		prefs.put(Constants.USER_DATA_KEY, userJSON.toJSONString());
	}

	@Override
	public StoredUserData load() {
		String userData = prefs.get(Constants.USER_DATA_KEY, Constants.DEFAULT_PREFERENCE_VALUE);
		JSONParser userParser = new JSONParser();
		StoredUserData u = null;
		try {
			JSONObject userJSONObj = (JSONObject) userParser.parse(userData);
			u = new StoredUserData();
			u.setUsername((String) userJSONObj.get(Constants.USERNAME_KEY));
			u.setSessionToken((String) userJSONObj.get(Constants.HEADER_SESSION_TOKEN));
			String activeProfileId = (String) userJSONObj.get(Constants.ACTIVE_PROFILE_ID_KEY);
			u.setActiveProfileId(new BigInteger(activeProfileId));
		} catch (Exception e) {
			return null;
		}
		return u;
	}

	@Override
	public void remove() {
		prefs.remove(Constants.USER_DATA_KEY);
	}
}
