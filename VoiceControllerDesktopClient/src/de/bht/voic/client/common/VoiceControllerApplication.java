package de.bht.voic.client.common;

import java.io.IOException;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import de.bht.voic.client.activities.AuthActivity;
import de.bht.voic.client.activities.ProfileActivity;
import de.bht.voic.client.activities.SpeechActivity;
import de.bht.voic.client.activities.StartActivity;
import de.bht.voic.client.views.SystemTrayView.StartPresenter;
import de.bht.voic.client.views.SystemTrayViewImpl;
import de.bht.voic.client.views.login.LoginView;
import de.bht.voic.client.views.login.LoginViewImpl;

@SuppressWarnings("restriction")
public class VoiceControllerApplication extends Application {

	@Override
	public void start(final Stage stage) {
		Platform.setImplicitExit(false);

		SystemTrayViewImpl systemTrayPanel = new SystemTrayViewImpl(stage);

		StartPresenter startActivity = new StartActivity(systemTrayPanel);

		javax.swing.SwingUtilities.invokeLater(systemTrayPanel);

		startActivity.checkAuth();

		ProfileActivity profileActivity = new ProfileActivity(systemTrayPanel);

		profileActivity.checkActiveProfile();
		profileActivity.userCanUpdateProfile();

		SpeechActivity speechActivity = new SpeechActivity(systemTrayPanel);

		LoginView loginPanel = new LoginViewImpl(stage);
		AuthActivity authActivity = new AuthActivity(loginPanel, systemTrayPanel);

		loginPanel.setPresenter(authActivity);

		systemTrayPanel.setPresenter(profileActivity);
		systemTrayPanel.setPresenter(speechActivity);
		systemTrayPanel.setPresenter(authActivity);
		systemTrayPanel.setPresenter(startActivity);

	}

	public static void main(String[] args) throws IOException, java.awt.AWTException {
		launch(args);
	}
}