package de.bht.voic.client.common;

import java.io.IOException;
import java.util.List;

import de.bht.voic.shared.model.rest.CLIAction;
import de.bht.voic.shared.model.rest.Command;
import de.bht.voic.shared.model.rest.KeyAction;
import de.bht.voic.shared.model.rest.TextAction;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.SpeechResult;

public class RecognitionThread extends Thread {

	private LiveSpeechRecognizer recognizer;
	private RobotKeyInputManager inputManager;
	private List<Command> commands;

	public RecognitionThread(LiveSpeechRecognizer recognizer, RobotKeyInputManager inputManager, List<Command> commands) {
		this.recognizer = recognizer;
		this.inputManager = inputManager;
		this.commands = commands;
	}

	@Override
	public void run() {
		// Start recognition process pruning previously cached data.
		recognizer.startRecognition(true);
		SpeechResult result = recognizer.getResult();

		while ((result = recognizer.getResult()) != null) {
			System.out.println(result.getHypothesis());
			for (Command command : commands) {
				String currentWord = command.getWord().toLowerCase();
				if (currentWord.equals(result.getHypothesis())) {

					if (command.getAction() instanceof KeyAction) {
						KeyAction action = (KeyAction) command.getAction();
						inputManager.stop();
						inputManager.start(action.getKeycodes(), action.getDelays());
					}

					if (command.getAction() instanceof TextAction) {
						TextAction action = (TextAction) command.getAction();
						RobotTextWriter writer = new RobotTextWriter();
						writer.write(action.getOs(), action.getText());
					}

					if (command.getAction() instanceof CLIAction) {
						CLIAction action = (CLIAction) command.getAction();
						executeCLIAction(action);
					}
				}
			}
		}
	}

	private void executeCLIAction(CLIAction action) {
		try {
			Runtime.getRuntime().exec(action.getExec());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void interrupt() {
		super.interrupt();
		recognizer.stopRecognition();
		inputManager.stop();
	}
}
