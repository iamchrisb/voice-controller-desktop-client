package de.bht.voic.client.common;

import java.math.BigInteger;

import de.bht.voic.client.model.StoredUserData;

public interface UserDataStorage {

	void store(final String sessionToken, final String username, final BigInteger activeProfileId);

	StoredUserData load();
	
	void remove();
	
}
