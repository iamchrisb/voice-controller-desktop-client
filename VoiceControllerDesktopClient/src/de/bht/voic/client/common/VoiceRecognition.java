package de.bht.voic.client.common;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;
import java.util.List;

import de.bht.voic.shared.model.rest.Command;
import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;

public class VoiceRecognition {
	private static final String LANGUAGE_MODEL_PATH = "resource:/edu/cmu/sphinx/models/en-us/en-us.lm.dmp";
	private static final String DICTIONARY_PATH = "resource:/edu/cmu/sphinx/models/en-us/cmudict-en-us.dict";
	private static final String ACOUSTIC_MODEL_PATH = "resource:/edu/cmu/sphinx/models/en-us/en-us";
	protected static final long RECORD_TIME = 1000;
	private LiveSpeechRecognizer recognizer;
	private RobotKeyInputManager inputManager;
	private RecognitionThread recognitionThread;

	public void doSpeech(List<Command> commands) throws IOException, AWTException {
		Configuration configuration = new Configuration();
		configuration.setAcousticModelPath(ACOUSTIC_MODEL_PATH);
		configuration.setDictionaryPath(DICTIONARY_PATH);
		configuration.setLanguageModelPath(LANGUAGE_MODEL_PATH);

		String path = new File(Constants.VC_SETTINGS_FOLDER).toURI().toURL().getPath();
		configuration.setGrammarPath(Constants.FILE_PROTOCOL + path);
		configuration.setGrammarName(Constants.GRAMMAR_NAME);

		configuration.setUseGrammar(true);

		recognizer = new LiveSpeechRecognizer(configuration);

		inputManager = new RobotKeyInputManager();

		recognitionThread = new RecognitionThread(recognizer, inputManager, commands);
		recognitionThread.start();

	}

	public void stopSpeech() {
		recognitionThread.interrupt();
	}
}
