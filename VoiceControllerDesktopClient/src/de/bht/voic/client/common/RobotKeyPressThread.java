package de.bht.voic.client.common;

import java.awt.AWTException;
import java.awt.Robot;

public class RobotKeyPressThread implements Runnable {

	private static final String NAME_TAG = "Name: ";
	private static final String KEYCODE_TAG = "Keycode: ";
	private static final String DELAY_TAG = "Delay: ";
	private static final String RATE_TAG = "Rate: ";
	
	private static final String START_MSG = "Starting KeyPressThread => ";
	private static final String STOP_MSG = "Stopping KeyPressThread => ";
	
	private static final String WHITESPACE = " ";
	
	private Thread t;
	private Robot robot;

	private String name;
	private Integer keycode;
	private Integer delayInMillis;
	private Integer rateInMillis;


	public RobotKeyPressThread(String name, Integer keycode, Integer delayInMillis, Integer rateInMillis) {
		this.name = name;
		this.keycode = keycode;
		this.delayInMillis = delayInMillis;
		this.rateInMillis = rateInMillis;
	}

	public void start() {
		System.out.println(START_MSG 
				+ NAME_TAG + name + WHITESPACE
				+ KEYCODE_TAG + keycode + WHITESPACE
				+ DELAY_TAG + delayInMillis + WHITESPACE
				+ RATE_TAG + rateInMillis + WHITESPACE);
		t = new Thread(this, "KeyPressThread_" + keycode);
		t.start();
	}

	public void stop() {
		System.out.println(STOP_MSG 
				+ NAME_TAG + name + WHITESPACE
				+ KEYCODE_TAG + keycode + WHITESPACE 
				+ DELAY_TAG + delayInMillis+ WHITESPACE
				+ RATE_TAG + rateInMillis + WHITESPACE);

		if(robot != null) {
			robot.keyRelease(keycode);			
		}
		if(t != null && t.isAlive() && !t.isInterrupted()) {
			t.interrupt();
		}
	}

	@Override
	public void run() {

		try {
			robot = new Robot();
		} catch (AWTException e1) {
			System.out.println("Cannot load Robot!");
			e1.printStackTrace();
		}
		
		Long endTime = System.currentTimeMillis() + delayInMillis;
		while (System.currentTimeMillis() <= endTime) {
			
			robot.keyPress(keycode);
			try {
				Thread.sleep(rateInMillis);
			} catch (InterruptedException e) {
				System.out.println("Sleep in Thread =>"
						+ name + " failed.");
			}
		}

	}
}
