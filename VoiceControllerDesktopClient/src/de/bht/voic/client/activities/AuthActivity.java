package de.bht.voic.client.activities;

import java.math.BigInteger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

import de.bht.voic.client.common.Constants;
import de.bht.voic.client.common.UserDataStorage;
import de.bht.voic.client.common.UserPreferenceStorage;
import de.bht.voic.client.views.SystemTrayView;
import de.bht.voic.client.views.login.LoginView;
import de.bht.voic.client.views.login.LoginView.AuthPresenter;
import de.bht.voic.shared.model.rest.AuthResponse;

public class AuthActivity implements AuthPresenter {

	private UserDataStorage userStorage;
	private LoginView loginView;
	private SystemTrayView systemTrayView;

	public AuthActivity(LoginView loginView, SystemTrayView systemTrayView) {
		this.loginView = loginView;
		this.systemTrayView = systemTrayView;
		userStorage = new UserPreferenceStorage();
	}

	@Override
	public void login(String username, String password) {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(Constants.REST_API_AUTH_LOGIN);

		Form form = new Form();
		form.param(Constants.USERNAME_KEY, username);
		form.param(Constants.PASSWORD_KEY, password);

		AuthResponse authResponse;

		try {
			authResponse = target.request(MediaType.APPLICATION_JSON).header(Constants.HEADER_API_KEY, Constants.API_KEY)
					.post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED), AuthResponse.class);
		} catch (Exception e) {
			loginView.showResponse("Error! It may be that there is no internet connection, please try again.");
			e.printStackTrace();
			return;
		}

		String sessionTokenRsp = authResponse.getSessionToken();
		String usernameRsp = authResponse.getUser().getName();

		BigInteger activeProfileIdRsp = authResponse.getUserInformation().getActiveProfileId();

		String responseMessage = "";

		if (activeProfileIdRsp == null) {
			responseMessage = "Please set an active profile online, before you log in!";
			loginView.showResponse(responseMessage);
			return;
		}
		userStorage.store(sessionTokenRsp, usernameRsp, activeProfileIdRsp);
		responseMessage = "You are now logged in.";

		systemTrayView.userIsLoggedIn(usernameRsp);
		systemTrayView.userCanNotUpdateProfile(false);
		loginView.showResponse(responseMessage);
	}

	@Override
	public void logout() {
		userStorage.remove();
	}

}
