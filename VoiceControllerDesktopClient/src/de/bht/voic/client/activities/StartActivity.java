package de.bht.voic.client.activities;

import de.bht.voic.client.common.UserDataStorage;
import de.bht.voic.client.common.UserPreferenceStorage;
import de.bht.voic.client.model.StoredUserData;
import de.bht.voic.client.views.SystemTrayView;
import de.bht.voic.client.views.SystemTrayView.StartPresenter;

public class StartActivity implements StartPresenter {

	private SystemTrayView systemTrayView;

	private UserDataStorage userStorage;

	public StartActivity(SystemTrayView systemTrayView) {
		this.systemTrayView = systemTrayView;
		userStorage = new UserPreferenceStorage();
	}

	@Override
	public void checkAuth() {
		StoredUserData load = userStorage.load();
		if (load == null) {
			systemTrayView.userIsLogedOut();
			return;
		}

		if (load.getUsername() == null || load.getUsername().isEmpty()) {
			systemTrayView.userIsLogedOut();
			return;
		}

		if (load.getSessionToken() == null || load.getSessionToken().isEmpty()) {
			systemTrayView.userIsLogedOut();
			return;
		}
		systemTrayView.userIsLoggedIn(load.getUsername());
	}
}
