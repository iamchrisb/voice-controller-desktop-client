package de.bht.voic.client.activities;

import java.awt.AWTException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import de.bht.voic.client.common.Constants;
import de.bht.voic.client.common.VoiceRecognition;
import de.bht.voic.client.views.SystemTrayView;
import de.bht.voic.client.views.SystemTrayView.ProfilePresenter;
import de.bht.voic.shared.model.rest.Command;

public class SpeechActivity implements SystemTrayView.SpeechPresenter {

	private static final String GRAMMAR_SEPPERATOR = " | ";
	private static final String NEW_LINE = "\n";

	private VoiceRecognition recognition;
	private ProfilePresenter profilePresenter;

	public SpeechActivity(SystemTrayView systemTrayView) {
		recognition = new VoiceRecognition();
		profilePresenter = new ProfileActivity(systemTrayView);
	}

	@Override
	public void startRecognition() {
		try {
			recognition.doSpeech(profilePresenter.readCommands());
		} catch (IOException | AWTException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void generateGrammatic() {
		List<Command> commands = profilePresenter.readCommands();

		try {
			StringBuffer grammar = new StringBuffer();

			String headerLine = "#JSGF V1.0;" + NEW_LINE;
			String grammarNameLine = "grammar " + Constants.GRAMMAR_NAME + ";" + NEW_LINE;
			String commandsLine = "public <command> = ( ";

			grammar.append(headerLine);
			grammar.append(grammarNameLine);
			grammar.append(commandsLine);

			for (Iterator<Command> iterator = commands.iterator(); iterator.hasNext();) {
				Command command = iterator.next();
				grammar.append(command.getWord().toLowerCase());

				if (iterator.hasNext()) {
					grammar.append(GRAMMAR_SEPPERATOR);
				}
			}

			grammar.append(" );");

			File grammarFile = new File(Constants.VC_GRAMMAR_PROFILE);
			BufferedWriter bwr = new BufferedWriter(new FileWriter(grammarFile, false));
			bwr.write(grammar.toString());
			bwr.flush();
			bwr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void stopRecognition() {
		recognition.stopSpeech();
	}

}
