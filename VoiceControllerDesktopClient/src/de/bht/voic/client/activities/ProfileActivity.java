package de.bht.voic.client.activities;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.bht.voic.client.common.Constants;
import de.bht.voic.client.common.UserDataStorage;
import de.bht.voic.client.common.UserPreferenceStorage;
import de.bht.voic.client.model.StoredUserData;
import de.bht.voic.client.views.SystemTrayView;
import de.bht.voic.shared.model.rest.Command;

public class ProfileActivity implements SystemTrayView.ProfilePresenter {

	private UserDataStorage userStorage;
	private ObjectMapper profileMapper;
	private SystemTrayView systemTrayView;

	public ProfileActivity(SystemTrayView systemTrayView) {
		this.systemTrayView = systemTrayView;
		userStorage = new UserPreferenceStorage();
		profileMapper = new ObjectMapper();
	}

	@Override
	public void fetchActiveProfile() {
		StoredUserData userData = userStorage.load();

		boolean isNotLoggedIn = userData == null || userData.getSessionToken() == null || userData.getSessionToken().isEmpty()
				|| userData.getUsername() == null || userData.getUsername().isEmpty();

		if (isNotLoggedIn) {
			return;
		}

		if (userData.getActiveProfileId() == null) {
			return;
		}

		List<Command> commands = null;

		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet(Constants.REST_API_COMMANDS + "?" + Constants.PROFILE_ID_KEY + "=" + userData.getActiveProfileId());
		request.addHeader(Constants.HEADER_API_KEY, Constants.API_KEY);
		request.addHeader(Constants.HEADER_SESSION_TOKEN, userData.getSessionToken());
		HttpResponse response;
		try {
			response = client.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = IOUtils.toString(rd);

			System.out.println(line);
			commands = profileMapper.readValue(line, new TypeReference<List<Command>>() {
			});

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		File profileFolder = new File(Constants.VC_SETTINGS_FOLDER);
		if (!profileFolder.exists()) {
			profileFolder.mkdir();
		}

		File profileFile = new File(Constants.VC_SETTINGS_PROFILE);

		try {
			profileMapper.writeValue(profileFile, commands);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		systemTrayView.userHasActiveProfile(profileFile.exists());
	}

	@Override
	public List<Command> readCommands() {
		List<Command> commands = null;
		try {
			commands = profileMapper.readValue(new File(Constants.VC_SETTINGS_PROFILE), new TypeReference<List<Command>>() {
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return commands;
	}

	@Override
	public void checkActiveProfile() {
		File settingsProfile = new File(Constants.VC_SETTINGS_PROFILE);
		systemTrayView.userHasActiveProfile(settingsProfile.exists());
	}

	@Override
	public void userCanUpdateProfile() {
		StoredUserData userData = userStorage.load();
		boolean canNotUpdate = userData == null || userData.getActiveProfileId() == null;
		systemTrayView.userCanNotUpdateProfile(canNotUpdate);
	}
}
