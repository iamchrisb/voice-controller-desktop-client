package de.bht.voic.client.model;

import java.math.BigInteger;

public class StoredUserData {

	private BigInteger activeProfileId;
	private String sessionToken;
	private String username;

	public StoredUserData(final String sessionToken, final String username, final BigInteger activeProfileId) {
		this.sessionToken = sessionToken;
		this.username = username;
		this.activeProfileId = activeProfileId;
	}

	public StoredUserData() {
		// JSON
	}

	public BigInteger getActiveProfileId() {
		return activeProfileId;
	}

	public void setActiveProfileId(BigInteger activeProfileId) {
		this.activeProfileId = activeProfileId;
	}

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
