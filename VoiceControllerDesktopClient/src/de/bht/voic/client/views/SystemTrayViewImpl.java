package de.bht.voic.client.views;

import java.io.IOException;
import java.util.Timer;

import javafx.application.Platform;
import javafx.stage.Stage;

import javax.imageio.ImageIO;

import de.bht.voic.client.views.login.LoginView.AuthPresenter;

public class SystemTrayViewImpl implements Runnable, SystemTrayView {

	private static final String EXIT = "Exit";
	private static final String LOGOUT = "Logout";
	private static final String LOGIN = "Login";

	private java.net.URL icon;

	private Timer notificationTimer = new Timer();

	@SuppressWarnings("restriction")
	private Stage stage;
	private SpeechPresenter speechPresenter;
	private ProfilePresenter profilePresenter;
	private StartPresenter startPresetner;
	private java.awt.MenuItem loginItem;
	private java.awt.MenuItem logoutItem;
	private AuthPresenter authPresenter;
	private java.awt.MenuItem updateProfileMenuItem;
	private java.awt.CheckboxMenuItem profileControlMenuItem;

	@SuppressWarnings("restriction")
	public SystemTrayViewImpl(Stage stage) {
		this.stage = stage;
		this.icon = SystemTrayViewImpl.class.getResource("icons/GameCenter-icon.png");
	}

	@SuppressWarnings("restriction")
	private void addAppToTray() {
		try {
			java.awt.Toolkit.getDefaultToolkit();

			if (!java.awt.SystemTray.isSupported()) {
				System.out.println("No system tray support, application exiting.");
				Platform.exit();
			}

			java.awt.SystemTray tray = java.awt.SystemTray.getSystemTray();
			java.awt.Image image = ImageIO.read(icon);
			java.awt.TrayIcon trayIcon = new java.awt.TrayIcon(image);

			trayIcon.addActionListener(event -> Platform.runLater(() -> {
				showStage();
			}));

			loginItem = new java.awt.MenuItem(LOGIN);
			loginItem.addActionListener(event -> Platform.runLater(() -> {
				showStage();
			}));

			logoutItem = new java.awt.MenuItem(LOGOUT);
			logoutItem.addActionListener(event -> Platform.runLater(() -> {
				logout();
			}));

			profileControlMenuItem = new java.awt.CheckboxMenuItem("Use active profile");

			profileControlMenuItem.addItemListener(event -> Platform.runLater(() -> {
				enableVoiceDeamon(profileControlMenuItem.getState());
			}));

			updateProfileMenuItem = new java.awt.MenuItem("update profile");

			updateProfileMenuItem.addActionListener(event -> Platform.runLater(() -> {
				updateProfile();
			}));

			java.awt.Font defaultFont = java.awt.Font.decode(null);
			java.awt.Font boldFont = defaultFont.deriveFont(java.awt.Font.BOLD);
			loginItem.setFont(boldFont);

			java.awt.MenuItem exitItem = new java.awt.MenuItem(EXIT);
			exitItem.addActionListener(event -> {
				notificationTimer.cancel();
				Platform.exit();
				tray.remove(trayIcon);
			});

			final java.awt.PopupMenu popup = new java.awt.PopupMenu();
			popup.add(profileControlMenuItem);
			popup.add(updateProfileMenuItem);
			popup.addSeparator();
			popup.add(loginItem);
			popup.add(logoutItem);
			popup.addSeparator();
			popup.add(exitItem);
			trayIcon.setPopupMenu(popup);

			// checkTime(trayIcon);

			tray.add(trayIcon);
		} catch (java.awt.AWTException | IOException e) {
			System.out.println("Unable to init system tray");
			e.printStackTrace();
		}
	}

	private void logout() {
		logoutItem.setEnabled(false);
		loginItem.setLabel("Login");
		loginItem.setEnabled(true);
		authPresenter.logout();
	}

	private void updateProfile() {
		profilePresenter.fetchActiveProfile();
		speechPresenter.generateGrammatic();
		profilePresenter.userCanUpdateProfile();
	}

	private void enableVoiceDeamon(boolean enabled) {
		if (enabled) {
			speechPresenter.startRecognition();
		} else {
			speechPresenter.stopRecognition();
		}
	}

	@SuppressWarnings("restriction")
	private void showStage() {
		if (stage != null) {
			stage.show();
			stage.toFront();
		}
	}

	@Override
	public void run() {
		addAppToTray();
	}

	@Override
	public void setPresenter(ProfilePresenter profilePresenter) {
		this.profilePresenter = profilePresenter;
	}

	@Override
	public void setPresenter(AuthPresenter authPresenter) {
		this.authPresenter = authPresenter;
	}

	@Override
	public void setPresenter(SpeechPresenter speechPresenter) {
		this.speechPresenter = speechPresenter;
	}

	@Override
	public void userIsLoggedIn(String username) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				loginItem.setEnabled(false);
				loginItem.setLabel("Logged in as: " + username);
				logoutItem.setEnabled(true);
			}
		});
	}

	@Override
	public void userIsLogedOut() {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				loginItem.setEnabled(true);
				loginItem.setLabel("Login");
				logoutItem.setEnabled(false);
			}
		});
	}

	@Override
	public void setPresenter(StartPresenter startPresenter) {
		this.startPresetner = startPresenter;
	}

	@Override
	public void userHasActiveProfile(boolean settingsExists) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				profileControlMenuItem.setEnabled(settingsExists);
			}
		});
	}

	@Override
	public void userCanNotUpdateProfile(boolean updatable) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				updateProfileMenuItem.setEnabled(!updatable);
			}
		});

	}
}
