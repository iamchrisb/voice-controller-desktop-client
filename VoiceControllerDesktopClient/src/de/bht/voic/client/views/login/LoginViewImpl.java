package de.bht.voic.client.views.login;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class LoginViewImpl implements LoginView {

	private AuthPresenter presenter;
	private VBox content;
	private Label responseLabel;

	@SuppressWarnings("restriction")
	public LoginViewImpl(final Stage stage) {
		StackPane layout = new StackPane(createContent());
		layout.setStyle("-fx-background-color: rgba(255, 255, 255, 1.0);");
		layout.setPrefSize(300, 200);

		layout.setOnMouseClicked(event -> stage.hide());

		Scene scene = new Scene(layout);

		stage.setScene(scene);
	}

	@SuppressWarnings("restriction")
	private Node createContent() {
		Label hello = new Label("Login");
		hello.setStyle("-fx-font-size: 40px; -fx-text-fill: forestgreen;");
		Label instructions = new Label("(click to hide)");
		instructions.setStyle("-fx-font-size: 12px; -fx-text-fill: orange;");

		content = new VBox(10);
		content.getChildren().addAll(hello, instructions);

		content.setAlignment(Pos.CENTER);
		Label usernameLabel = new Label("username");
		TextField usernameTextField = new TextField("");
		Label passwordLabel = new Label("password");
		PasswordField passwordTextField = new PasswordField();

		Button loginBtn = new Button("login");

		loginBtn.setOnAction(event -> {
			presenter.login(usernameTextField.getText(), passwordTextField.getText());
		});

		content.getChildren().addAll(usernameLabel, usernameTextField, passwordLabel, passwordTextField, loginBtn);

		responseLabel = new Label("");
		content.getChildren().add(responseLabel);

		return content;
	}

	@Override
	public void setPresenter(AuthPresenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void showResponse(String message) {
		responseLabel.setText(message);
	}

}
