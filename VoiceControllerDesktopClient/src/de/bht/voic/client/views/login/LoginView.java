package de.bht.voic.client.views.login;

public interface LoginView {

	public interface AuthPresenter {
		void login(String username, String password);

		void logout();
	}

	void setPresenter(AuthPresenter authActivity);

	void showResponse(String message);
}