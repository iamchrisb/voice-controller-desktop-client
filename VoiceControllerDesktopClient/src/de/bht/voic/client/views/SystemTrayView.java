package de.bht.voic.client.views;

import java.util.List;

import de.bht.voic.client.views.login.LoginView.AuthPresenter;
import de.bht.voic.shared.model.rest.Command;

public interface SystemTrayView {

	public interface ProfilePresenter {
		void fetchActiveProfile();

		List<Command> readCommands();

		void checkActiveProfile();

		void userCanUpdateProfile();
	}

	public interface StartPresenter {
		void checkAuth();
	}

	public interface SpeechPresenter {
		void startRecognition();

		void stopRecognition();

		void generateGrammatic();
	}

	void setPresenter(ProfilePresenter profilePresenter);

	void setPresenter(SpeechPresenter speechPresenter);

	void setPresenter(StartPresenter startPresenter);

	void userIsLogedOut();

	void userIsLoggedIn(String username);

	void setPresenter(AuthPresenter authPresenter);

	void userHasActiveProfile(boolean exists);

	void userCanNotUpdateProfile(boolean updatable);
}
